#!/bin/bash

mkdir -p ./in
mkdir -p ./out
if [[ ! -f "./watermark.png" ]]; then
	echo "Ensure that ./watermark.png exists."
	exit 1
fi

shopt -s nullglob

if [[ ! -z $1 ]]; then
	max_jobs=$1
else
	max_jobs=$(grep ^cpu\\scores /proc/cpuinfo | uniq | awk '{print $4}')
fi

echo "Using $max_jobs workers. Jobs will report when they finish."

function wait_for_jobs() {
	while [[ $(jobs | wc -l) -ge $max_jobs ]]; do
		sleep 0.01
	done
}

#FILES=$"$(find in -type f -follow)"
SPACES="                                                                                    "

find in -type f -follow | while read f; do
	OUT=$(echo $f | sed s/in/out/)
	if [[ ! -f "$OUT" ]]; then
		if [[ $(file "$f") == *"image"* ]]; then
			wait_for_jobs
			DIR=$(dirname "$OUT")
			mkdir -p "$DIR"
			composite -gravity Center watermark.png "$f" "$OUT" &&
				printf "\r$SPACES\rFinished job $f => $OUT" &
		else
			printf "\r$SPACES\rSkipped $f, not an image"
		fi
	fi
done
wait
sleep 1s
echo
echo "All jobs complete"
